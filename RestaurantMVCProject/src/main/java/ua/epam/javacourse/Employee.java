package ua.epam.javacourse;

public class Employee {
    private int id;
    private String name;
    private int restaurantId;

    public Employee(int id, String name, int restaurantId) {
        this.id = id;
        this.name = name;
        this.restaurantId = restaurantId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Override
    public String toString() {
        return "Employee: " +
                "id=" + id +
                ", name=" + name +
                ", restaurantId=" + restaurantId;
    }
}

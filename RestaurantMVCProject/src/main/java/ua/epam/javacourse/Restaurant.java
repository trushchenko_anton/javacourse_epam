package ua.epam.javacourse;

public class Restaurant {
    private int id;
    private String name;
    private String city;
    private int capacity;
    private int ownerId;
    private int chefId;
    private boolean childrenArea;

    public Restaurant(int id, String name, String city, int capacity, int ownerId, int chefId, int childrenArea) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.capacity = capacity;
        this.ownerId = ownerId;
        this.chefId = chefId;

        if(childrenArea == 1)
            this.childrenArea = true;
        else
            this.childrenArea = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getChefId() {
        return chefId;
    }

    public void setChefId(int chefId) {
        this.chefId = chefId;
    }

    public boolean isChildrenArea() {
        return childrenArea;
    }

    public void setChildrenArea(boolean childrenArea) {
        this.childrenArea = childrenArea;
    }

    @Override
    public String toString() {
        return "Restaurant: " +
                "id=" + id +
                ", name=" + name +
                ", city=" + city +
                ", capacity=" + capacity +
                ", ownerId=" + ownerId +
                ", chefId=" + chefId +
                ", childrenArea=" + childrenArea;
    }
}

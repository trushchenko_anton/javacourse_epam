package ua.epam.javacourse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RequestToDB {
    private static String URL_ADDRESS = "jdbc:h2:d:\\restaurants";
    private static String USER_NAME = "admin";
    private static String PASSWORD = "admin";
    Connection connection;

    private void openConnection() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(
                    URL_ADDRESS, USER_NAME, PASSWORD);

            System.out.println("Connected to database");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Restaurant> getAllRestaurants(){
        List<Restaurant> restaurants = new ArrayList<Restaurant>();
        openConnection();
        try {
            Statement st = connection.createStatement();

            String sql = "SELECT * FROM RESTAURANTS";

            ResultSet rs = st.executeQuery(sql);

            while (rs.next())
                restaurants.add(new Restaurant(rs.getInt("ID"),
                        rs.getString("NAME"), rs.getString("CITY"),
                        rs.getInt("CAPACITY"), rs.getInt("OWNERID"),
                        rs.getInt("CHEFID"), rs.getInt("CHILDRENAREA")));

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();

        return restaurants;
    }

    public List<Owner> getAllOwners(){
        List<Owner> owners = new ArrayList<Owner>();
        openConnection();
        try {
            Statement st = connection.createStatement();

            String sql = "SELECT * FROM OWNERS";

            ResultSet rs = st.executeQuery(sql);

            while (rs.next())
                owners.add(new Owner(rs.getInt("ID"), rs.getString("NAME")));

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();

        return owners;
    }

    public List<Employee> getAllEmployees(){
        List<Employee> employees = new ArrayList<Employee>();
        openConnection();
        try {
            Statement st = connection.createStatement();

            String sql = "SELECT * FROM EMPLOYEES";

            ResultSet rs = st.executeQuery(sql);

            while (rs.next())
                employees.add(new Employee(rs.getInt("ID"), rs.getString("NAME"),
                        rs.getInt("RESTAURANTID")));

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();

        return employees;

    }

    public void getAllData(){
        getAllRestaurants();
        getAllOwners();
        getAllEmployees();
    }

    public void addRestaurant(int id, String name, String city, int capacity, int ownerId, int chefId, int childrenArea){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "INSERT INTO RESTAURANTS (ID, NAME, CITY, CAPACITY, OWNERID, CHEFID, CHILDRENAREA) " +
                    "VALUES ("+ id +", '"+ name +"','"+ city +"', "+ capacity +", "+ ownerId +", "+ chefId +", "+ childrenArea +")";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void addOwner(int id, String name){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "INSERT INTO OWNERS (ID, NAME) VALUES ("+ id +", '"+ name +"')";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void addEmployee(int id, String name, int restaurantID){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "INSERT INTO EMPLOYEES (ID, NAME, RESTAURANTID) VALUES ("+ id +", '"+ name +"', "+ restaurantID +")";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void deleteRestaurant(int id){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "DELETE FROM RESTAURANTS WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void deleteEmployee(int id){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "DELETE FROM EMPLOYEES WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void deleteOwner(int id){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "DELETE FROM OWNERS WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void updateRestaurant(int id, String name, String city, int capacity, int ownerId, int chefId, int childrenArea){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "UPDATE RESTAURANTS SET NAME='"+ name +"', CITY='"+ city +"', CAPACITY="+ capacity +
                    ",OWNERID="+ ownerId +", CHEFID="+ chefId +", CHILDRENAREA="+ childrenArea +" WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void updateOwner(int id, String name){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "UPDATE OWNERS SET NAME='"+ name +"' WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public void updateEmployee(int id, String name, int restaurantId){
        openConnection();

        try {
            Statement st = connection.createStatement();

            String sql = "UPDATE EMPLOYEES SET NAME='"+ name +"', RESTAURANTID="+ restaurantId +" WHERE ID="+ id +"";

            st.execute(sql);

        } catch (SQLException e){
            e.printStackTrace();
        }

        closeConnection();
    }

    public boolean isChef(int id){
        List<Restaurant> restaurants = getAllRestaurants();

        for (Restaurant restaurant : restaurants)
            if (restaurant.getChefId() == id)
                return true;

        return false;
    }

    public List<Restaurant> hasChildrenArea(){
        List<Restaurant> restaurants = getAllRestaurants();
        List<Restaurant> restaurantsWithChildrenArea = new ArrayList<Restaurant>();

        for (Restaurant restaurant : restaurants)
            if (restaurant.isChildrenArea() == true)
                restaurantsWithChildrenArea.add(restaurant);

        return  restaurantsWithChildrenArea;
    }

    public List<Restaurant> hasNoChildrenArea(){
        List<Restaurant> restaurants = getAllRestaurants();
        List<Restaurant> restaurantsWithoutChildrenArea = new ArrayList<Restaurant>();

        for (Restaurant restaurant : restaurants)
            if (restaurant.isChildrenArea() == false)
                restaurantsWithoutChildrenArea.add(restaurant);

        return  restaurantsWithoutChildrenArea;
    }

    public int numOfEmployees (int id){
        int numOfEmployees = 0;

        List<Employee> employees = getAllEmployees();

        for (Employee employee : employees)
            if (employee.getRestaurantId() == id)
                numOfEmployees++;

        return numOfEmployees;
    }

    public int numOfRestaurants (int id){
        int numOfRestaurants = 0;

        List<Restaurant> restaurants = getAllRestaurants();

        for (Restaurant restaurant : restaurants)
            if (restaurant.getOwnerId() == id)
                numOfRestaurants++;

        return numOfRestaurants;
    }

    public int numOfRestaurants (String city){
        int numOfRestaurants = 0;

        List<Restaurant> restaurants =getAllRestaurants();

        for (Restaurant restaurant : restaurants)
            if (restaurant.getCity().equals(city))
                numOfRestaurants++;

        return numOfRestaurants;
    }

}

package ua.epam.javacourse.servlets;

import ua.epam.javacourse.RequestToDB;
import ua.epam.javacourse.Restaurant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "TestServlet")
public class TestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw =response.getWriter();

        RequestToDB rtd = new RequestToDB();

        pw.println("<style>");
        pw.println("td {");
        pw.println("text-align: center;");
        pw.println("}");
        pw.println("</style>");

        pw.println("<table style=\"width:100%\">");
        pw.println("<tr>");
        pw.println("<th >Id</th>");
        pw.println("<th>Name</th>");
        pw.println("<th>City</th>");
        pw.println("<th>Capacity</th>");
        pw.println("<th>OwnerId</th>");
        pw.println("<th>ChefId</th>");
        pw.println("<th>ChildrenArea</th>");
        pw.println("</tr>");

        for (Restaurant r : rtd.getAllRestaurants()) {

            pw.println("<tr>");
            pw.println("<td>"+r.getId()+"</td>");
            pw.println("<td>"+r.getName()+"</td>");
            pw.println("<td>"+r.getCity()+"</td>");
            pw.println("<td>"+r.getCapacity()+"</td>");
            pw.println("<td>"+r.getOwnerId()+"</td>");
            pw.println("<td>"+r.getChefId()+"</td>");
            pw.println("<td>"+r.isChildrenArea()+"</td>");
            pw.println("</tr>");

        }

        pw.println("</table>");

    }
}
